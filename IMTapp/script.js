let heightInputField = document.getElementById("heightInputField");
let weightInputField = document.getElementById("weightInputField");

let imtOutputField = document.getElementById("imtOutputField");
let resultOutputField = document.getElementById("resultOutputField");

function Calculate() 
{
    let height = parseFloat(heightInputField.value);
    let weight = parseFloat(weightInputField.value);
    
    height /= 100;
    
    let imt = weight / (height*height);

    let result;

    if(imt<=16)
    {
        result = "Выраженный дефицит массы тела";
    }
    else if (imt>16 && imt<=18.5)
    {
        result = "Выраженный дефицит массы тела";
    }
    else if (imt>18.5 && imt<=24,99)
    {
        result = "Норма";
    }
    else if (imt>25 && imt<=30)
    {
        result = "Избыточная масса тела (предожирение)";
    }
    else if (imt>30 && imt<=35)
    {
        result = "Ожирение";
    }
    else if (imt>35 && imt<=40)
    {
        result = "Ожирение резкое";
    }
    else if (imt <= 40)
    {
        result = "Очень резкое ожирение";
    }
    
    imtOutputField.innerHTML = "IMT: "+imt;
    resultOutputField.innerHTML = "Result: "+result;


}

function Clear()
{
    heightInputField.value = "";
    weightInputField.value = "";
    imtOutputField.innerHTML = "IMT: ";
    resultOutputField.innerHTML = "Result: ";
    
}